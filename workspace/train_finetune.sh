#!/usr/bin/env bash
die(){
    echo >&2 "$@"
    exit 1
}

echo $@
echo "Usage: train_finetune.sh config_file_name.json model_file_name.ckpt [additional_options]"

CONFIG_FILE=$1
shift
MODEL_FILE=$1
shift
ADDITIONAL_OPTIONS="$@"

my_dir="$(dirname "$0")"
. $my_dir/set_env.sh
echo "MMAR_ROOT set to $MMAR_ROOT"
echo "CONFIG_FILE set to $CONFIG_FILE"
echo "MODEL_FILE set to $MODEL_FILE"
echo "ADDITIONAL_OPTIONS set to $ADDITIONAL_OPTIONS"

export TF_ENABLE_AUTO_MIXED_PRECISION=0

python3 -u  -m nvmidl.apps.train \
    -m $MMAR_ROOT \
    -c $MMAR_ROOT/config/$CONFIG_FILE \
    --set \
     DATA_ROOT="/workspace/data" \
     DATASET_JSON="/workspace/config/dataset.json" \
     PROCESSING_TASK="segmentation" \
     MMAR_CKPT_DIR="/output" \
     MMAR_EVAL_OUTPUT_PATH="/output" \ 
     PRETRAIN_WEIGHTS_FILE="" \
     MMAR_CKPT=/workspace/model/$MODEL_FILE \
     multi_gpu=false \
     use_amp=false  ${additional_options}
