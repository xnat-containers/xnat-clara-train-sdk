#!/bin/bash

# nvmidl-dataconvert -d /input [OUTPUT_RESOLUTION] -s .dcm -e .nii -o /output [IS_MASK]

die(){
    echo >&2 "$@"
	echo DICOM_DIR=$DICOM_DIR
	echo OUTPUT_DIR=$OUTPUT_DIR
	echo OUTPUT_FILENAME=$OUTPUT_FILENAME
	echo OTHER_ARGS=$OTHER_ARGS
	echo UN_DICOM=$UN_DICOM
	
    exit 1
}

echo "$@"

if [ $# -lt 4 ]
then
    printf "Not enough arguments - %d\n" $# 
	die "run.sh DICOM_DIR OUTPUT_DIR OUTPUT_FILENAME OTHER_ARGS"
fi
	

DICOM_DIR=$1; shift
OUTPUT_DIR=$1; shift
OUTPUT_FILENAME=$1; shift
OTHER_ARGS=$@

echo DICOM_DIR=$DICOM_DIR
echo OUTPUT_DIR=$OUTPUT_DIR
echo OUTPUT_FILENAME=$OUTPUT_FILENAME
echo OTHER_ARGS=$OTHER_ARGS

MASK_FLAG='-l'

if [[ "$OTHER_ARGS" == *"$MASK_FLAG"* ]]; then

     nvmidl-dataconvert -d $DICOM_DIR -o $OUTPUT_DIR $OTHER_ARGS

  else

    echo "Checking for enhanced DICOM - i.e. single .dcm file"
    DCM_COUNT=`ls -1q $DICOM_DIR/*.dcm | wc -l`
    echo DCM_COUNT=$DCM_COUNT
    UN_DICOM=
    if [ $DCM_COUNT -lt 1 ]
    then
      die "Could not find any DICOM files."
    fi
    if [ $DCM_COUNT -lt 2 ]
    then
      echo "One DICOM image file found. Assuming Enhanced DICOM. I need to un-enhance this before converting to NIFTI."
      UN_DICOM=$OUTPUT_DIR/$OUTPUT_FILENAME/
      mkdir $UN_DICOM
      echo UN_DICOM=$UN_DICOM
      dcuncat -unenhance -noconcat $DICOM_DIR/*.dcm  -of $UN_DICOM || die "dicom3tools.dcuncat failed to un-enhance input dicom"
      DCM_COUNT=`ls -1q $UN_DICOM/*.dcm | wc -l`
      echo DCM_COUNT=$DCM_COUNT
      if [ $DCM_COUNT -lt 1 ]
      then
        echo "Adding .dcm extension."
        for file in $UN_DICOM/*; do mv "$file" "$file.dcm"; done
      fi
      echo "DICOM files split and saved to " $UN_DICOM
      export DICOM_DIR=$OUTPUT_DIR
    else
      echo "Multiple DICOM files found, no need to un-enhance.  Creating input directory link to guide output file name."
      ln -s $DICOM_DIR $OUTPUT_DIR/$OUTPUT_FILENAME
      export DICOM_DIR=$OUTPUT_DIR
    fi

    echo "Converting DICOM input to NIFTI using image files in " + $DICOM_DIR

    echo "nvmidl-dataconvert -d "  $DICOM_DIR " -o " $OUTPUT_DIR $OTHER_ARGS
    nvmidl-dataconvert -d $DICOM_DIR -o $OUTPUT_DIR $OTHER_ARGS


    if [[ ! -z "$UN_DICOM" ]]
    then
      echo "Removing temporary dicom files in" $UN_DICOM
      rm -rf $UN_DICOM
    else
            rm  $OUTPUT_DIR/$OUTPUT_FILENAME
    fi

fi

